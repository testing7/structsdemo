#include <iostream>

struct SimplestStruct
{
    int field_00 = 100;
    int field_04 = 1337;
    int field_08 = INT_MAX;

    void PrintFieldsMeticulously()
    {
        printf("== SimplestStruct ==\n\tBase address: %lx\n\t[%lx] field_00: %d\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\n",
            this,
            &field_00, field_00,
            &field_04, field_04,
            &field_08, field_08);
    }
};

struct AlignedStruct
{
    int field_00 = 100;
    int field_04 = 1337;
    int field_08 = INT_MAX;
    unsigned char field_12 = 37;
    int field_16 = -2;

    void PrintFieldsMeticulously()
    {
        printf("== AlignedStruct ==\n\tBase address: %lx\n\t[%lx] field_00: %d\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\t[%lx] field_12: %u\n\t[%lx] field_16: %d\n\n",
            this,
            &field_00, field_00,
            &field_04, field_04,
            &field_08, field_08,
            &field_12, field_12,
            &field_16, field_16);
    }
};

#pragma pack(1)
struct PackedStruct
{
    int field_00 = 100;
    int field_04 = 1337;
    int field_08 = INT_MAX;
    unsigned char field_12 = 37;
    int field_13 = -2;

    void PrintFieldsMeticulously()
    {
        printf("== PackedStruct ==\n\tBase address: %lx\n\t[%lx] field_00: %d\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\t[%lx] field_12: %u\n\t[%lx] field_13: %d\n\n",
            this,
            &field_00, field_00,
            &field_04, field_04,
            &field_08, field_08,
            &field_12, field_12,
            &field_13, field_13);
    }
};

struct StructWithPointers
{
    int field_00 = 100;
    int field_04 = 1337;
    int field_08 = INT_MAX;
    const char* field_12 = "This is a string";
    int* field_16 = 0;

    StructWithPointers() { field_16 = &field_04; }

    void PrintFieldsMeticulously()
    {
        printf("== StructWithPointers ==\n\tBase address: %lx\n\t[%lx] field_00: %d\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\t[%lx] field_12: %lx (%s)\n\t[%lx] field_16: %lx (%d)\n\n",
            this,
            &field_00, field_00,
            &field_04, field_04,
            &field_08, field_08,
            &field_12, (std::uintptr_t)field_12, field_12, /* <-- we cast to uintptr_t to convince the printf function to output the actual value of the pointer. this is specific to how printf handles strings (char pointers / char arrays) */
            &field_16, field_16, *field_16);
    }
};

struct SimplestStructWithInheritance : SimplestStruct
{
    int field_12 = 888;
    int field_16 = 1546;

    void PrintFieldsMeticulously()
    {
        printf("== SimplestStructWithInheritance ==\n\tBase address: %lx\n\t[%lx] field_00: %d\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\t[%lx] field_12: %d\n\t[%lx] field_16: %d\n\n",
            this,
            &field_00, field_00,
            &field_04, field_04,
            &field_08, field_08,
            &field_12, field_12,
            &field_16, field_16);
    }
};

struct StructWithVTable
{
    int field_04 = 100;
    int field_08 = 1337;
    int field_12 = INT_MAX;
    const char* field_16 = "This is another string";
    int* field_20 = 0;

    StructWithVTable() { field_20 = &field_08; }

    virtual void Print() { printf("null"); }
    virtual void Set() { };
    virtual int VFunc_03(int param) { return param + 1; }

    virtual void PrintFieldsMeticulously()
    {
        printf("== StructWithVTable ==\n\tBase address: %lx\n\t[%lx] VTable: %lx\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\t[%lx] field_12: %d\n\t[%lx] field_16: %lx (%s)\n\t[%lx] field_20: %lx (%d)\n\n",
            this,
            this, *(std::uintptr_t*)this, /* <-- the vtable is technically field_00, it's implicitly set by the compiler */
            &field_04, field_04,
            &field_08, field_08,
            &field_12, field_12,
            &field_16, (std::uintptr_t)field_16, field_16, /* <-- we cast to uintptr_t to convince the printf function to output the actual value of the pointer. this is specific to how printf handles strings (char pointers / char arrays) */
            &field_20, field_20, *field_20);
    }
};

struct StructWithVTable_WithInheritance : StructWithVTable
{
    StructWithVTable_WithInheritance() : StructWithVTable() {}

    // overrides (2 of the 4) virtual methods defined in base class
    int VFunc_03(int param) { return param + 2; }
    void PrintFieldsMeticulously()
    {
        printf("== StructWithVTable_WithInheritance ==\n\tBase address: %lx\n\t[%lx] VTable: %lx\n\t[%lx] field_04: %d\n\t[%lx] field_08: %d\n\t[%lx] field_12: %d\n\t[%lx] field_16: %lx (%s)\n\t[%lx] field_20: %lx (%d)\n\n",
            this,
            this, *(std::uintptr_t*)this, /* <-- the vtable is technically field_00, it's implicitly set by the compiler */
            &field_04, field_04,
            &field_08, field_08,
            &field_12, field_12,
            &field_16, (std::uintptr_t)field_16, field_16, /* <-- we cast to uintptr_t to convince the printf function to output the actual value of the pointer. this is specific to how printf handles strings (char pointers / char arrays) */
            &field_20, field_20, *field_20);
    }
};

int main()
{
    // all of these structs are allocated on the stack. this means they'll all be really close to each other, and all be really close to the value of ESP if you attach a debugger and set a breakpoint.
    // you could allocate them on the heap by using 'new', this will return a pointer to the address of the allocated memory for the instance. since we allocated the space, we should deallocate it when we're done, by using 'delete'.
    // from a low-level perspective, there is no difference between using 'new' to allocate on the heap and using this format to allocate on the stack, the end result is that space is created for our instance and we get a pointer to that space.
    // it's just that when we allocate on the stack, the 'pointer' isn't something we see at this high-level, it's the compiler automatically keeping track of the stack (via the stack pointer register, ESP/RSP).
    // to illustrate that last point: if you look at the disassembly for this function, you'll see before each call to each struct's ::PrintFieldsMeticulously() function "mov ecx, [esp+SomeOffset]"
    // haha jk, all the ::PrintFieldsMeticulously() functions get inlined due to compiler optimizations. still, passing each field to the printf call is done via push [esp+someOffset+fieldOffset], the [esp+someOffset..] is akin to how
    // our pointer returned by 'new' would work: the compiled assembly would be very similar. if this seems convoluted, don't worry about it.
    // you don't need to know how compilers generate assembly from code that uses the heap vs code that uses the stack (what i was trying to explain above), but it is important to know the general concept of a stack and a heap.
    SimplestStruct simplestStruct = SimplestStruct();
    AlignedStruct alignedStruct = AlignedStruct();
    PackedStruct packedStruct = PackedStruct();
    StructWithPointers structWithPointers = StructWithPointers();
    SimplestStructWithInheritance structWithInheritance = SimplestStructWithInheritance();
    StructWithVTable structWithVTable = StructWithVTable();
    StructWithVTable_WithInheritance structWithVTable_WithInheritance = StructWithVTable_WithInheritance();

    simplestStruct.PrintFieldsMeticulously();
    alignedStruct.PrintFieldsMeticulously();
    packedStruct.PrintFieldsMeticulously();
    structWithPointers.PrintFieldsMeticulously();
    structWithInheritance.PrintFieldsMeticulously();
    structWithVTable.PrintFieldsMeticulously();
    structWithVTable_WithInheritance.PrintFieldsMeticulously();

    // just to pause the app
    std::cout << "Press ENTER to exit...\n";
    getchar();
    return 0;
}